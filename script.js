$(window).ready(function(){
	bindEvent()
});
	
//change this to match your own server path
var host = "http://codesix.de/nestedforms/"

//registers click event to the button
function bindEvent(){
	var button = $('#load')
	button.click(function(e){	
		var elem = $('#level_1').val();
		loadContent(elem);
		return false;
	});
};

//load the content from the subdirectory
function loadContent(elem){
	$.ajax({
		url : host + 'content/level2.' + elem + '.htm',
		success : function(data){
			$(data).insertAfter('#level_1');
		},
		error : function(){
			alert('Oooops. Something went wrong.')
		}
	})
};
